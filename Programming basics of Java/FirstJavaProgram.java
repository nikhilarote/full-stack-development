public class FirstJavaProgram {

   /* This is my first java program.
    * This will print 'Hello World' as the output
    */
   // salary  variable is a private static variable
         private static double salary;

   // DEPARTMENT is a constant
      public static final String DEPARTMENT = "Development ";
 
   public void age() {
      int a = 7;
      for(int i = 0; i < 7; i++) {
         a++;
         System.out.println("Increment of a " + a);
      }
   }

   public static void main(String []args) {
      System.out.println("Hello World"); // prints Hello World

      
      salary = 1000;
      System.out.println(DEPARTMENT + "average salary:" + salary);

      FirstJavaProgram instance = new FirstJavaProgram(); // Object created
      instance.age();  // Using object method called  
   }
}